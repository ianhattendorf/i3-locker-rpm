Name:		i3-locker
Version:	0.0.1
Release:	1%{?dist}
Summary:	Screen locker

License:	None
URL:		https://gitlab.com/ianhattendorf/i3-locker
# Note: Source0 needs to be updated for every release
Source0:	https://gitlab.com/ianhattendorf/i3-locker/-/archive/v%{version}/i3-locker-v%{version}.tar.gz

BuildRequires:	gcc-c++
BuildRequires:	make
BuildRequires:	cmake
BuildRequires:	dbus-devel
BuildRequires:	libxcb-devel
BuildRequires:	cereal-devel
BuildRequires:	ImageMagick-c++-devel

%description
Screen locker, heavily custimized, using i3lock-color

%prep
%setup -q -n %{name}-v%{version}

%build
./linux-build-dependencies.sh -O
./linux-build-make.sh -tnb Release
cd build
%{cmake} ..
make %{?_smp_mflags}

%install
%make_install

%files
%doc README.md
#%license LICENSE.md
%{_bindir}/i3-locker

%changelog
* Sun May 13 2018 Ian Hattendorf <ian@ianhattendorf.com> - 0.0.1-1
- Initial RPM spec

